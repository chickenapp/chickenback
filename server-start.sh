#!/bin/bash
/usr/local/bin/python -m pip install --upgrade pip
python3 -m venv venv
source venv/bin/activate
pip install --no-cache-dir -r requirements.txt
pip install opencv-python
python init_db.py
uvicorn main:app --reload --host 0.0.0.0 --port 3000