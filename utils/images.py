import base64, cv2
import uuid
import shutil
import os
import pathlib
from dotenv import load_dotenv

load_dotenv()

class Images():
    def get_default_image():
        img = cv2.imread(str(pathlib.Path().resolve()) + str(os.getenv('IMAGES_PATH'))  + str(os.getenv('IMAGE_DEFAULT')))
        jpg_img = cv2.imencode('.png', img)
        content_string = base64.b64encode(jpg_img[1]).decode('utf-8')
        return content_string

    def get_b64_image(file):
        filename = str(uuid.uuid4())
        file_location = str(pathlib.Path().resolve()) + str(os.getenv('IMAGES_PATH')) + f"{filename}.png"
        with open(file_location, "wb+") as file_object:
            shutil.copyfileobj(file.file, file_object)
        img = cv2.imread(file_location)
        str_image = cv2.imencode('.png', img)
        content_string = base64.b64encode(str_image[1]).decode('utf-8')
        if os.path.exists(file_location):
            os.remove(file_location)
        return content_string