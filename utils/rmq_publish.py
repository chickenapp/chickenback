import os,re
import pika
from dotenv import load_dotenv
from config.log import logging as lg

load_dotenv()

class RabbitMQ:
    def send_message(message ,queue_name,action,key = None):
        if  str(os.getenv('RMQ_ACTIVE')) == 'true':
            try:
                connection = pika.BlockingConnection( pika.ConnectionParameters(host=str(os.getenv('IP_RMQ')),port=str(os.getenv('PORT_RMQ'))))
                channel = connection.channel()
                channel.exchange_declare(exchange=queue_name,
                                                    exchange_type='topic',
                                                    durable=True,
                                                    auto_delete=False)
                if key:
                    value_name=queue_name+'_'+ action + '_' + key
                else:
                    value_name=queue_name+'_'+ action
                channel.queue_declare(queue=value_name,
                                          durable=True)
                channel.queue_bind(queue=value_name,exchange=queue_name,routing_key=value_name)
                channel.basic_publish(exchange=queue_name,
                                    routing_key=value_name,
                                    body=str(message).replace('\'','"').replace('True','true').replace('False','false'),
                                    properties=pika.BasicProperties(
                                    delivery_mode=2)
                                    )
                connection.close()
            except Exception as e:
                lg.error(e)