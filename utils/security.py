import os
from fastapi import HTTPException, Header
from dotenv import load_dotenv

load_dotenv()

async def verify_key(Api_Key: str = Header(...)):
    if Api_Key != os.getenv('API_KEY_DEVELOP'):
        raise HTTPException(status_code=400, detail="X-Key header invalid")
    return Api_Key

async def verify_token(Bearer_Token: str = Header(...)):
    if Bearer_Token != os.getenv('API_KEY_DEVELOP'):
        raise HTTPException(status_code=401, detail="Token Bearer invalid")
    return Bearer_Token
