from threading import local
from database import SessionLocal
from models.configuration_model import Config

db= SessionLocal()

def if_printer_is_active():
    local_printer=db.query(Config).with_entities(Config.ip_local_printer).first()
    if local_printer:
        return local_printer
    return None


