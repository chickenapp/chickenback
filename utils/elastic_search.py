from datetime import datetime
import os
from config.log import logging
from config import elastic
from dotenv import load_dotenv

load_dotenv()

class Elastic_Index:
    def send_document(model, model_name = None, action=None):
        if str(os.getenv('ELASTIC_ACTIVE')) == 'true':
            try:
                logging.debug(model.serialize())
                index_doc = 'chicken4u_'
                if model_name:
                    index_doc += model_name + '_'
                if action:
                    index_doc += action + '_'
                index_doc += str(datetime.today().strftime('%Y_%m_%d'))
                elastic.es.index(index=index_doc, id=model.id, document=model.serialize())
            except Exception as e:
                logging.debug(e)
                index_doc ='chicken4u_logs_'+ str(datetime.today().strftime('%Y_%m_%d'))
                elastic.es.index(index=index_doc, id=model.id, document=model.serialize())
