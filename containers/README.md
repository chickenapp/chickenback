[![N|Solid](https://img.freepik.com/free-vector/chicken-mascot-logo-design-vector-template_441059-159.jpg)](https://nodesource.com/products/nsolid)
# Chicken 4 U Containers
## Deployment de toda la solución basada en contenedores Docker.

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Antes de poder deployar los contenedores hemos de asegurarnos tener docker y docker-compose levantado en nuestro sistema.
Para ello lanzaremos el siguiente comando saber la versión actual de Docker y Docker Compose:

```sh
docker --version
docker-compose --version
```

# Generación del contenedor ***back-api***
En este caso vamos a generar al vuelo el contenedor de nuestro backend, ya que de momento no existe una copia en ningún repositorio Docker.

Para ello nos situamos dentro de la carpeta del backend, y asegurarnos que el archivo Dockerfile existe y lanzamos el siguiente comando.

```sh
  docker build -t back-api .
```

Esto nos genera un contenedor con nuestro proyecto entero. Cuando hagamos un nuevo desarrollo o feature, simplemente paramos toda la solución y volvemos a lanzar el mismo comando.

# `ATENCIÓN!!`
Si cuando generamos el contenedor cambiamos su nombre deberemos cambiarlo también en el `docker-compose.yml`
# Creación de volúmenes para los contenedores

```sh
docker volume create portainer_data
docker volume create uptime-kuma
```

##### Levantar todos los contenedores
Vamos a proceder a levantar los contenedores pero antes hemos de asegurarnos que la configuración del archivo `containers/.env` tenga correctamente definida la clave `BACKEND_PATH` y es necesario para poder levantar el proyecto.

`BACKEND_PATH`=ruta donde tenemos el proyecto back
`POSTGRES_USER`=usuario de la db
`POSTGRES_PASSWORD`=pass de la db
`POSTGRES_DB`=nombre de la db

Una vez hayamos rellenado los campos correctamente iniciamos el proceso de levantar todo. Para ello vamos a la ruta donde tenemos el proyecto BackEnd. ¿Por qué?

Hemos mapeado el contenedor con una ruta local, para que una vez levantado el Docker, podamos desarrollar dentro del contenedor en un entorno venv, y así tener todo el entorno desacoplado de una máquina física.

Como hemos configurado el servicio API en modo `--reload`, cada vez que guardemos cualquier cambio se refrescará el proyecto (a excepción de nuevos paquetes y/o configuraciones que para ellos, deberemos reiniciar el contenedor individualmente)

##### Levantar el proyecto
- Primero entramos en la carpeta **containers** del proyecto backend
  - Dentro veremos el archivos `docker-compose.yml` y el `.env` *(oculto)*
- Comando que lanzamos en la terminal
  ```sh
  docker-compose -f docker-compose.yml up
  ```
Este comando lo que hace es leer la configuración de todos los servicios que hemos declarado en el archivo **YML**. El parámetro `-f` sirve para indicarle que la configuración está en ese archivo, por si queremos lanzar el comando desde otra ruta (en ese caso deberíamos especificar la ruta donde se encuentra nuestra archivo **YML**)

Los **logs** que vemos en la terminal son de todos los servicios que compone el proyecto, por eso si queremos, podemos lanzar el mismo proyecto pero en modo background para que la terminal no vaya mostrando los logs.

- Primero eliminariamos el proyecto lanzado anteriormente.
```sh
  docker-compose -f docker-compose.yml down -v
```

- Seguidamente levantamos todo otra vez pero en modo background con el flag `-d`
```sh
  docker-compose -f docker-compose.yml up -d
```

Comandos docker/docker-compose más usados:
##### Levanta todo el proyecto mostrando los logs en la terminal
En ubuntu podemos hacer un `Ctrl+z` y pasar esos logs en segundo plano. Para volver a verlo escribimos en la terminal `fg` y recuperamos la traza

```sh
  docker-compose -f docker-compose.yml up
```
##### Levanta todo el proyecto en modo `dettached`
Así no lanza los logs, solo devuelve el estado

```sh
  docker-compose -f docker-compose.yml up -d
```

##### Elimina el proyecto pero no los vólumenes asociados
Deja los datos en persistencia

```sh
  docker-compose -f docker-compose.yml down
```

##### Elimina el proyecto y los vólumenes asociados
No deja los datos en persistencia

```sh
  docker-compose -f docker-compose.yml down -v
```

##### Para el proyecto
Para cualquier proyecto levantado con docker-compose
```sh
  docker-compose -f docker-compose.yml stop
```
##### Inicia un proyecto parado
Desde un archivo *YML*
```sh
  docker-compose -f docker-compose.yml start
```

##### Reinicia un proyecto
Reinicia cualquier proyecto levantado con docker-compose
```sh
  docker-compose -f docker-compose.yml restart
```

##### Crea un volumen de datos para nuestras aplicaciones
Para mantener persistencia
```sh
  docker volume create mi_volumen
```

##### Lista todos los volumenes creados en docker
Hay que ir eliminando lo que no se use, ya que ocupa espacio en disco
```sh
  docker volume ls
```
##### Crea un network
Para que podamos compartir la conexión entre containers

```sh
  docker network create -d bridge minetwork
```

##### Lista todas las network creadas en docker
No eliminar las networks por defecto **host o bridge**

```sh
  docker network ls
```

##### Lista todos los containers
Sólo los contenedores activos

```sh
  docker container ls
```

##### Lista todos los containers
Los activos y no activos

```sh
  docker container ls -a
```

##### Lista todas las imágenes
Interesante ya que nos muestra las imágenes con sus tags, espacio en disco ...
```sh
  docker image ls
```

##### Genera un contenedor
A partir de una archivo `Dockerfile` y lo hace con el tag `latest`

```sh
  docker build -t back-api .
```

##### Genera un contenedor a partir de una archivo `Dockerfile` y lo hace con el tag `dev`
Cambiamos *dev* por nuestro tag y listos

```sh
  docker build -t back-api:dev .
```

##### Logs de un contenedor por CONTAINER ID
Muestra en la terminal las últimas 1000 líneas del log de un contenedor por `CONTAINER ID`

```sh
  docker logs --tail 1000 -f 5f470aeb1d57
```

##### Logs de un contenedor por CONTAINER NAME
Muestra en la terminal las últimas 1000 líneas del log de un contenedor por `CONTAINER NAME`

```sh
  docker logs --tail 1000 -f back-api
```

##### ELiminar imágenes "huérfanas"
Elimina todas aquellas imágenes que no tienen tag y no son referenciadas por otro container

```sh
  docker image prune
```

##### ELiminar imágenes que no son referenciadas por ningun contenedor existente
Elimina todas aquellas imágenes que no son referenciadas por otro container

```sh
  docker image prune -a
```

##### ELiminar imágenes que no son referenciadas por ningun contenedor existente y han sido parados más de N horas
Elimina todas aquellas imágenes que no son referenciadas por otro container

```sh
  docker image prune -a --filter "until=24h"
  docker image prune -a --filter "until=48h"
  ...
```

Todas las sentencias de prune son aplicables a:
- Image
- Container
- Network
- Volume

Referencia: [Docker prune][docker-prune]

[docker-prune]: <https://docs.docker.com/config/pruning/>

