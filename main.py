from fastapi import FastAPI, HTTPException, Header
from fastapi_pagination import add_pagination
from database import SessionLocal
from dotenv import load_dotenv
from routers import auth, configuration, customer, order_type, orders, product, product_type, qr, status_order, unit_measure, alive

load_dotenv()

app =FastAPI(
             title="chicken4U",
    description="Applicación de gestión polleril",
    version="0.0.1",
    contact={
        "name": "Cono Llansola",
        "email": "cono1@chicken4U.com",
    })

app.include_router(auth.router)
app.include_router(alive.router)
app.include_router(product.router)
app.include_router(product_type.router)
app.include_router(order_type.router)
app.include_router(status_order.router)
app.include_router(customer.router)
app.include_router(unit_measure.router)
app.include_router(orders.router)
app.include_router(qr.router)
app.include_router(configuration.router)

db= SessionLocal()

add_pagination(app)