import json
from sqlalchemy.sql.expression import cast
import os,rsa
from random import randrange
from typing import List
import sqlalchemy
from database import Base, engine, SessionLocal
from utils import images
from models.customer_model import Customer
from models.product_model import Product
from models.unit_measure_model import Unit_Measure
from models.status_model import  Status
from models.order_type_model import Order_Type
from models.product_type_model import Product_Type
from models.order_header_model import Order_Header
from models.order_detail_model import Order_Detail
from models.user_model import User, Role
from models.configuration_model import Config
# from models.menu_header_model import Menu_Header
# from models.menu_detail_model import Menu_Detail
from faker import Faker
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.schema import CreateSchema, DropSchema
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.ext.serializer import loads, dumps
import time

t = time.process_time()

publicKey, privateKey = rsa.newkeys(512)

print('Drop database and data ....')
Base.metadata.drop_all(engine)

print('Creating new Schema')
if engine.dialect.has_schema(engine,'chickenapp1'):
    print('Previous schema dropped')
    engine.execute(DropSchema('chickenapp1'))
engine.execute(CreateSchema('chickenapp1'))
print('Schema created')

print('Creating database ....')
Base.metadata.create_all(engine)
if not database_exists(engine.url):
    create_database(engine.url)
if not engine.dialect.has_schema(engine, os.getenv('DB_SCHEMA')):
    engine.execute(sqlalchemy.schema.CreateSchema(os.getenv('DB_SCHEMA')))

db= SessionLocal()

faker = Faker()

demo_uuid = open('demo_data/demo_uuid.txt', "w")

object_config= [
    Config(num_ticket=0)
]
db.add_all(object_config)
db.commit()

# User
object_user=[
    # User(name='David', password=rsa.encrypt('1234'.encode(),publicKey),email='cono1@chicken4u.com'),
    # User(name='Christian', password=rsa.encrypt('1234'.encode(),publicKey),email='cono2@chicken4u.com'),
    # User(name='Enric', password=rsa.encrypt('1234'.encode(),publicKey),email='cono3@chicken4u.com')
    User(name='David', password='1234',email='cono1@chicken4u.com'),
    User(name='Christian', password='1234',email='cono2@chicken4u.com'),
    User(name='Enric', password='1234',email='cono3@chicken4u.com')
]
db.add_all(object_user)
db.commit()

object_role=[
    Role(name='Administrador'),
    Role(name='Mostrador'),
    Role(name='Cocina'),
    Role(name='Backoffice')
]
db.add_all(object_role)
db.commit()

usuario1 = db.query(User).filter(User.name =='David').first()
usuario2 =  db.query(User).filter(User.name =='Christian').first()
usuario3 =  db.query(User).filter(User.name =='Enric').first()
role_admin= db.query(Role).filter(Role.name =='Administrador').first()
role_mostrador= db.query(Role).filter(Role.name =='Mostrador').first()
role_cocina= db.query(Role).filter(Role.name =='Cocina').first()
role_backoffice= db.query(Role).filter(Role.name =='Backoffice').first()

demo_uuid.write('############################  USERS  ############################' + '\n')
demo_uuid.write(str(usuario1.id) + '\n')
demo_uuid.write(str(usuario2.id)+ '\n')
demo_uuid.write(str(usuario3.id)+ '\n\n')
demo_uuid.write('############################  ROLES  ############################' + '\n')
demo_uuid.write(str(role_admin.id) + '\n')
demo_uuid.write(str(role_cocina.id)+ '\n')
demo_uuid.write(str(role_backoffice.id)+ '\n')
demo_uuid.write(str(role_mostrador.id)+ '\n\n')

usr =usuario1
rl = role_admin
usr.roles.append(rl)
db.add(rl)
db.commit()

usr =usuario1
rl = role_backoffice
usr.roles.append(rl)
db.add(rl)
db.commit()

usr =usuario2
rl = role_cocina
usr.roles.append(rl)
db.add(rl)
db.commit()

usr =usuario2
rl = role_backoffice
usr.roles.append(rl)
db.add(rl)
db.commit()

usr =usuario3
rl = role_mostrador
usr.roles.append(rl)
db.add(rl)
db.commit()

usr =usuario3
rl = role_cocina
usr.roles.append(rl)
db.add(rl)
db.commit()


# Unit of Measure
object_measure=[
    Unit_Measure(name='KG'),
    Unit_Measure(name='LT'),
    Unit_Measure(name='GR'),
    Unit_Measure(name='UND'),
    Unit_Measure(name='HL'),
    Unit_Measure(name='ML'),
    ]
db.add_all(object_measure)
db.commit()

# Customers
object_customer=[]
for i in range(200):
    object_customer.append(Customer(name=faker.name(),phone=faker.phone_number(),email=faker.email()))

db.add_all(object_customer)
db.commit()

# Status
object_status=[
    Status(name='Abierto', key_name='open',isDefault=True),
    Status(name='En proceso', key_name='in_process', isDefault=False),
    Status(name='Preparar', key_name='prepare', isDefault=False),
    Status(name='Entregado', key_name='delivered', isDefault=False),
    Status(name='Cobrado', key_name='charge', isDefault=False),
    Status(name='Cerrado', key_name='close', isDefault=False),
    ]
db.add_all(object_status)
db.commit()

# Order Types
object_order_type=[
    Order_Type(name='Telefonico', description=faker.text(), isDefault=True),
    Order_Type(name='Email',description=faker.text(), isDefault=False),
    Order_Type(name='Mostrador',description=faker.text(), isDefault=False),
    ]

db.add_all(object_order_type)
db.commit()

# Product Type
object_product_type=[
    Product_Type(name='Base', isStock=True ),
    Product_Type(name='Complemento',isStock=False),
    ]

db.add_all(object_product_type)
db.commit()

# all units measure ID
um_all=[dict(u) for u in db.query(Unit_Measure.id).all()]
demo_uuid.write('############################  UNITS OF MEASURE  ############################' + '\n')
demo_uuid.write(str(um_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(um_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# all product types ID
product_type_all=[dict(u) for u in db.query(Product_Type.id).all()]
demo_uuid.write('############################  PRODUCTS TYPE  ############################' + '\n')
demo_uuid.write(str(product_type_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(product_type_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# all customer  ID
customers_all=[dict(u) for u in db.query(Customer.id).all()]
demo_uuid.write('############################  CUSTOMERS  ############################' + '\n')
demo_uuid.write(str(customers_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(customers_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(customers_all[2]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(customers_all[3]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# all status  ID
status_all=[dict(u) for u in db.query(Status.id).all()]
demo_uuid.write('############################  STATUS  ############################' + '\n')
demo_uuid.write(str(status_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(status_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(status_all[3]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# all order type  ID
order_type_all=[dict(u) for u in db.query(Order_Type.id).all()]
demo_uuid.write('############################  ORDERS TYPE  ############################' + '\n')
demo_uuid.write(str(order_type_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_type_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_type_all[2]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# Product
object_product=[]
for i in range(100):
    um_uuid_selected=(list(um_all)[randrange(len(um_all)-1)])
    product_type_uuid_selected=(list(product_type_all)[randrange(len(product_type_all))])
    object_product.append(Product(
                                product_image=images.Images.get_default_image(),
                                name=faker.company(),
                                description=faker.text(),
                                price=faker.pyfloat(positive=True, right_digits=2, left_digits=2),
                                quantity=faker.random_int(),
                                warning_stock=faker.random_int(),
                                um_uuid=um_uuid_selected.get('id'),
                                product_type_uuid=product_type_uuid_selected.get('id'),
                                ))

db.add_all(object_product)
db.commit()

# all prodcuts ID
product_all=[dict(u) for u in db.query(Product.id).all()]
demo_uuid.write('############################  PRODUCTS  ############################' + '\n')
demo_uuid.write(str(product_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(product_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(product_all[10]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

# Order Header
object_order_header=[]
for i in range(200):
    customer_uuid_selected=(list(customers_all)[randrange(len(customers_all))])
    order_type_uuid_selected=(list(order_type_all)[randrange(len(order_type_all)-1)])
    status_uuid_selected=(list(status_all)[randrange(len(status_all)-1)])
    object_order_header.append(Order_Header(
                                entry_date=faker.date_time(),
                                delivery_date=faker.date_time(),
                                customer_uuid=customer_uuid_selected.get('id'),
                                order_type_uuid=order_type_uuid_selected.get('id'),
                                order_status_uuid=status_uuid_selected.get('id'),
                                total=faker.pyfloat(positive=True, right_digits=3, left_digits=2),
                                ))
db.add_all(object_order_header)
db.commit()

# all Order Headers
order_header_all=[dict(u) for u in db.query(Order_Header.id).all()]
demo_uuid.write('############################  ORDERS HEADER  ############################' + '\n')
demo_uuid.write(str(order_header_all[0]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_header_all[1]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_header_all[2]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_header_all[3]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

print('Número de cabeceras de PEDIDOS .... ',len(order_header_all))

object_order_details=[]
for i in range(len(order_header_all)):
    order_header_uuid_selected=str(order_header_all[i].get('id'))
    product_uuid_selected=(list(product_all)[randrange(len(product_all)-1)])
    for z in range(10):
        object_order_details.append(Order_Detail(
                                order_header_uuid=order_header_uuid_selected,
                                product_uuid=product_uuid_selected.get('id'),
                                price=faker.random_int(0,30),
                                quantity=faker.random_int(0,20),
                                ))
    db.add_all(object_order_details)
db.commit()


order_detail_all=[dict(u) for u in db.query(Order_Detail.id).all()]
demo_uuid.write('############################  ORDERS DETAIL  ############################' + '\n')
demo_uuid.write(str(order_detail_all[10]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_detail_all[21]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_detail_all[32]).replace("{'id': UUID('",'').replace("')}",'') + '\n')
demo_uuid.write(str(order_detail_all[33]).replace("{'id': UUID('",'').replace("')}",'') + '\n\n')

demo_uuid.close()

rsa_keys = open('rsa_keys/publicKey.txt', "a")
rsa_keys.write(str(publicKey))
rsa_keys.close()
rsa_keys = open('rsa_keys/privateKey.txt', "a")
rsa_keys.write(str(privateKey))
rsa_keys.close()

# rsa.decrypt(encMessage, privateKey).decode()

db.flush()
db.close()

print('********** Tiempo de ejecución DB **********  ',str(time.process_time() - t), '.segundos  ************************************************')



