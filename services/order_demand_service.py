import base64
import os
from fpdf import FPDF
from database import SessionLocal
from models.order_detail_model import Order_Detail
from models.order_header_model import Order_Header
from models.product_model import Product
from utils.print import if_printer_is_active
from utils.elastic_search import Elastic_Index as ei
from utils.rmq_publish import RabbitMQ as rmq

db= SessionLocal()

def get_pdf(order_header_uuid):
    header=db.query(Order_Header).filter(Order_Header.id == order_header_uuid).first()
    detail=db.query(Order_Detail).join(Product).with_entities(
                                                            Product.name.label('product_name'),
                                                            Order_Detail.quantity.label('product_quantity')
                                                            ).filter(Order_Detail.order_header_uuid == order_header_uuid).all()
    pdf = FPDF('P','mm',[90, 160])
    pdf.add_page()
    pdf.set_font('Courier','',12)
    pdf.image('resources/reports/logo_bk.png',10 , 4, 25)
    pdf.set_y(34)
    pdf.set_x(5)
    pdf.cell(200,16, 'Pedido: ' +  str(header.delivery_date), 0, 1, 'L', 0)
    pdf.set_x(2)
    pdf.set_y(40)
    pdf.cell(10,16, 'Cant', 0, 0, 'L')
    pdf.cell(40,16, 'Descrip.', 0, 1, 'C', 0)
    axiss_y = 46
    for item in detail:
      pdf.set_x(0)
      pdf.set_y(axiss_y)
      pdf.cell(6,16, str(item.product_quantity), 0, 0, 'L', 0)
      pdf.cell(4,16, '|', 0, 0, 'L', 0)
      pdf.cell(40,16, str(item.product_name), 0, 1, 'L', 0)
      axiss_y += 6
    pdf.output('resources/reports/' + order_header_uuid + '.pdf', 'F')
    with open('resources/reports/' + order_header_uuid + '.pdf', "rb") as pdf_file:
      content_base64 = base64.b64encode(pdf_file.read())
    printer=if_printer_is_active() or None
    status_response = 'to print'
    if printer:
          os.system('lp -d ' + str(printer.ip_local_printer) + ' resources/reports/' + order_header_uuid + '.pdf')
          status_response = 'printed'
    ei.send_document(header,'orders','demand_pdf')
    rmq.send_message(header.serialize(),'orders','demand_pdf')
    return {'status': status_response, 'content': content_base64}