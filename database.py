import os
import sqlalchemy
from sqlalchemy.orm import declarative_base
from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
from sqlalchemy_utils import database_exists,create_database

load_dotenv()

engine = create_engine("postgresql://"+ str(os.getenv('DB_USERNAME')) +":"+str(os.getenv('DB_PASSWORD')) +"@"+ str(os.getenv('IP_DB_CONNECTION')) + "/" + str(os.getenv('DB_NAME')),
                       echo=True)
if not database_exists(engine.url):
    create_database(engine.url)
if not engine.dialect.has_schema(engine, os.getenv('DB_SCHEMA')):
    engine.execute(sqlalchemy.schema.CreateSchema(os.getenv('DB_SCHEMA')))

Base=declarative_base()

Base.metadata.create_all(engine)

SessionLocal = sessionmaker(bind=engine)