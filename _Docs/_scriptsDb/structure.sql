create database chicken4u;

create table Productos(
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
descripcion varchar(255),
tipo_producto_uuid varchar(255),
cantidad float,
price float,
aviso_rotura_stock float,
um_uuid varchar(255),
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Tipo_Producto(
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
esStock boolean default true
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Pedidos_Cabecera (
id uuid PRIMARY KEY default uuid_generate_v4(),
fecha_realizado date,
fecha_entrega date,
cliente_uuid varchar(255),
tipo_pedido_uuid varchar(255),
estado_uuid varchar(255),
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Estados(
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Pedidos_Detalle (
id uuid PRIMARY KEY default uuid_generate_v4(),
pedido_cabecera_uuid varchar(255),
cliente_uuid varchar(255),
precio float,
producto_uuid varchar(255),
cantidad float,
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Tipo_Pedido(
id uuid PRIMARY KEY default uuid_generate_v4(),
name varchar(255),
descripcion varchar(255),
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Clientes(
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
telefono varchar(255),
email varchar(255),
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Menus_Cabecera (
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
precio float,
precio float,
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Menus_Detalle(
id uuid PRIMARY KEY default uuid_generate_v4(),
menu_cabecera_uuid varchar(255),
producto_uuid uuid,
cantidad float,
isOptional  boolean default false
createdAt timestamp,
modifiedAt timestamp,
activo boolean default true
);

create table Unidades_Medida(
id uuid PRIMARY KEY default uuid_generate_v4(),
nombre varchar(255),
descripcion varchar(255),
conversion float,
activo boolean default true
);

create table Configuraciones(
id uuid PRIMARY KEY default uuid_generate_v4(),
);