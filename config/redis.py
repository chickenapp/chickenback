import json
import os
from warnings import catch_warnings
import redis
from config.log import logging as lg

class Redis_Cluster:
    def register_data(id, action, body = None):
        if str(os.getenv('REDIS_ACTIVE')) == 'true':
            try:
                redis_client = redis.Redis(host=str(os.getenv('IP_REDIS')), port=int(os.getenv('PORT_REDIS')), db=0, decode_responses=True)
                if action == 'delete':
                    redis_key = redis_client.keys(pattern='*' + str(id))
                    if redis_key:
                        redis_client.delete(str(redis_key[0]))
                if action == 'update':
                    redis_key = redis_client.keys(pattern='*' + str(id))
                    if redis_key:
                        redis_client.set(redis_key[0], json.dumps(body))
                if action == 'create':
                    redis_client.set(id, json.dumps(body))
            except redis.ConnectionError as e:
                lg.error(e)

