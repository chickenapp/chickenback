import logging
from utils.obtain_format_date import obtain_format_date as od

filename ='logs/chicken4U_'+ str(od.year_eng())  +'.log'
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)
root_logger.setLevel(logging.ERROR)
root_logger.setLevel(logging.WARNING)
handler = logging.FileHandler(filename, 'a', 'utf-8')
handler.setFormatter(formatter)
root_logger.addHandler(handler)