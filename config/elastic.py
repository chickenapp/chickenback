import os
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch(str(os.getenv('IP_ELASTICSEARCH_DOCKER'))+':'+str(os.getenv('PORT_ELASTICSEARCH_DOCKER')))