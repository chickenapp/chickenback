FROM python:3.9.7
RUN apt-get update && apt-get install -y \
    software-properties-common

RUN apt-get upgrade --yes

RUN apt-get install nano
RUN apt-get install git

RUN apt-get update && apt-get install -y \
    python3.4 \
    python3-pip \
    python3-virtualenv \
    python3-venv --no-install-recommends --yes 

RUN apt-get update && apt-get install -y python3-opencv

WORKDIR /usr/src/app

COPY . .

EXPOSE 3000/tcp

CMD ["./server-start.sh"]