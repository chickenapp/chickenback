# README #

This README would normally document whatever steps are necessary to get your application up and running.


### Virtual Envoironment
Hacer gitclone del repositirio, acceder a la ruta base y ejecutar:

## Si no tenemos virutalenv de python
sudo apt install python3-virtualenv python3-venv


virtualenv venv
source venv/bin/activate


### Instalación de dependencias y paquetes
Desde la raiz y habiendo entrado en el entorno virtual ejecutamos:

pip install -r requierements.txt

Esto instalará los paquetes que se deben utilizar

### Levantar proyecto
uvicorn  main:app --reload

Acceso a Swagger:
http://localhost:8000/docs
ó
http://127.0.0.1:8000/docs

### ElasticSearch docker

docker pull docker.elastic.co/elasticsearch/elasticsearch:7.17.0

docker run --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.17.0

### Kibana docker

docker pull docker.elastic.co/kibana/kibana:7.17.0

docker run --link elasticsearch:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.17.0

https://www.youtube.com/watch?v=OJ5VQWZjaD8


* Accedeer a dashboard
http://localhost:5601
ó
http://127.0.0.1:5601


### RabbitMQ docker

docker pull rabbitmq:3-management

docker run --name rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq:3-management

* Accedeer a dashboard
http://localhost:15672
ó
http://127.0.0.1:15672

Usuario: guest
Password: guest


# Branches

  Rama principal main-dockers
  Rama desarrollo development
  Rama main(deprecated) main