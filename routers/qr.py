
from fastapi import APIRouter, Depends, Response,status, Response
from utils.security import verify_key
from serializers.customer_serializer import *
import qrcode

router = APIRouter()

@router.get('/qr/{code_uuid}',
            tags=["Qr"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_qur_code(response:Response,code_uuid:str):
    img=qrcode.make(str(code_uuid))
    type(img)
    img.save('resources/qr_codes/'+ str(code_uuid) + '.png')
    return None


