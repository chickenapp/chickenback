
from fastapi import APIRouter, Depends, HTTPException, Response,status, Response
from database import SessionLocal
from models.configuration_model import Config
from utils.security import verify_key
from serializers.customer_serializer import *

router = APIRouter()

db= SessionLocal()

@router.get('/config/counter',
            tags=["Configuration"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_next_ticket(response:Response):
    to_update=db.query(Config).first()
    if to_update is  None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Error to access number of ticket")
    to_update.num_ticket += 1
    db.commit()
    response.headers["Location"] = str(to_update.num_ticket)
    return None

@router.get('/config/reset',
            tags=["Configuration"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_reset():
    to_update=db.query(Config).first()
    to_update.num_ticket =0
    db.commit()
    return None