from operator import and_
from fastapi import APIRouter, Depends,status,HTTPException, Response
from fastapi_pagination import Page, paginate
from serializers.config_serializer import Config_Request
from serializers.status_serializer import Status_Request
from services import order_demand_service
from utils.check_uuid import Check_uuid
from utils.dates import get_current_date
from utils.print import if_printer_is_active
from utils.security import verify_key
from utils.rmq_publish import RabbitMQ as rmq
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.order_header_model import Order_Header
from models.order_detail_model import Order_Detail
from models.order_type_model import Order_Type
from models.product_model import Product
from models.customer_model import Customer
from models.status_model import Status
from serializers.order_serializer import Order_Detail_Response, Order_Pdf_Response, Order_Request, Order_Response
from serializers.product_type_serializer import *
from utils.elastic_search import Elastic_Index as ei
from config.redis import Redis_Cluster as rc
from fpdf import FPDF
import base64
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/orders', tags=["Orders"],
            response_model=Page[Order_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)] )
def get_all(response:Response):
    orders=db.query(Order_Header).join(Customer
                                            ).join(Order_Type
                                            ).join(Status
                                            ).with_entities(
                                                        Order_Header.id,
                                                        Order_Header.entry_date,
                                                        Order_Header.delivery_date,
                                                        Customer.id.label('customer_id'),
                                                        Customer.name.label('customer_name'),
                                                        Order_Type.name.label('order_type'),
                                                        Status.name.label('order_status'),
                                                        Order_Header.total,
                                                        Order_Header.created_at,
                                                        Order_Header.active
                                                      ).all()
    return paginate(orders)

@router.get('/orders/{order_header_uuid}',
            tags=["Orders"],
            response_model=Order_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)] )
def get_by_id(response:Response, order_header_uuid:str):
    order=db.query(Order_Header).join(Customer
                                            ).join(Order_Type
                                            ).join(Status
                                            ).with_entities(
                                                        Order_Header.id,
                                                        Order_Header.entry_date,
                                                        Order_Header.delivery_date,
                                                        Customer.id.label('customer_id'),
                                                        Customer.name.label('customer_name'),
                                                        Order_Type.name.label('order_type'),
                                                        Status.name.label('order_status'),
                                                        Order_Header.total,
                                                        Order_Header.created_at,
                                                        Order_Header.active
                                                      ).filter(Order_Header.id == order_header_uuid).first()
    return order

@router.post('/orders/header/actives',
             tags=["Orders"],
             response_model=Page[Order_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_header_actives(response:Response):
    order=db.query(Order_Header).join(Customer
                                            ).join(Order_Type
                                            ).join(Status
                                            ).with_entities(
                                                        Order_Header.id,
                                                        Order_Header.entry_date,
                                                        Order_Header.delivery_date,
                                                        Customer.id.label('customer_id'),
                                                        Customer.name.label('customer_name'),
                                                        Order_Type.name.label('order_type'),
                                                        Status.name.label('order_status'),
                                                        Order_Header.total,
                                                        Order_Header.created_at,
                                                        Order_Header.active
                                                      ).filter(Order_Header.active == True).all()
    return paginate(order)

@router.post('/orders/header/inactives',
             tags=["Orders"],
             response_model=Page[Order_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_header_inactives(response:Response):
    order=db.query(Order_Header).join(Customer
                                            ).join(Order_Type
                                            ).join(Status
                                            ).with_entities(
                                                        Order_Header.id,
                                                        Order_Header.entry_date,
                                                        Order_Header.delivery_date,
                                                        Customer.id.label('customer_id'),
                                                        Customer.name.label('customer_name'),
                                                        Order_Type.name.label('order_type'),
                                                        Status.name.label('order_status'),
                                                        Order_Header.total,
                                                        Order_Header.created_at,
                                                        Order_Header.active
                                                      ).filter(Order_Header.active == False).all()
    return paginate(order)

@router.get('/orders/detail/{order_header_uuid}',
            tags=["Orders"],
            response_model=Page[Order_Detail_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)] )
def get_all(response:Response, order_header_uuid:str):
    detail=db.query(Order_Detail).join(Product
                                            ).with_entities(
                                                        Order_Detail.id,
                                                        Product.id.label('product_uuid'),
                                                        Product.name.label('product_name'),
                                                        Order_Detail.price,
                                                        Order_Detail.quantity,
                                                        Order_Detail.created_at,
                                                        Order_Detail.active
                                                      ).filter(Order_Detail.order_header_uuid == order_header_uuid).all()
    return paginate(detail)

@router.post('/orders/detail/actives',
             tags=["Orders"],
             response_model=Page[Order_Detail_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_actives(response:Response):
    detail=db.query(Order_Detail).join(Product
                                            ).with_entities(
                                                        Order_Detail.id,
                                                        Product.id.label('product_uuid'),
                                                        Product.name.label('product_name'),
                                                        Order_Detail.price,
                                                        Order_Detail.quantity,
                                                        Order_Detail.created_at,
                                                        Order_Detail.active
                                                      ).filter(Order_Detail.active == True).all()
    return paginate(detail)

@router.post('/orders/detail/inactives',
             tags=["Orders"],
             response_model=Page[Order_Detail_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_inactives(response:Response):
    detail=db.query(Order_Detail).join(Product
                                            ).with_entities(
                                                        Order_Detail.id,
                                                        Product.id.label('product_uuid'),
                                                        Product.name.label('product_name'),
                                                        Order_Detail.price,
                                                        Order_Detail.quantity,
                                                        Order_Detail.created_at,
                                                        Order_Detail.active
                                                      ).filter(Order_Detail.active == False).all()
    return paginate(detail)

@router.post('/orders',
             tags=["Orders"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, order:Order_Request):
    try:
        new_order_header=Order_Header(
        id=Generate_Uuid.generate_new_uuid(),
        entry_date=order.entry_date,
        delivery_date=order.delivery_date,
        customer_uuid=order.customer_uuid,
        order_type_uuid=order.order_type_uuid,
        order_status_uuid =order.status_uuid,
        total=order.total
        )
        db.add(new_order_header)
        db.commit()
        customer = db.query(Customer).filter(Customer.id == order.customer_uuid).first()
        rmq_message ={'delivery_date': str(new_order_header.delivery_date), 'customer_id': str(customer.id), 'customer_name': customer.name}
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    try:
      detail_node =[]
      rmq_message_detail=[]
      for item in order.detail:
        detail_node.append(Order_Detail(
          id=Generate_Uuid.generate_new_uuid(),
          order_header_uuid=new_order_header.id,
          product_uuid=item.product_uuid,
          price=item.price,
          quantity=item.quantity
        ))
        product_name = db.query(Product).filter(Product.id == item.product_uuid).first()
        rmq_message_detail.append({'product_id': str(product_name.id),'product_name': product_name.name, 'quantity': item.quantity})
      db.add_all(detail_node)
      db.commit()
      rmq_message.update({'detail': rmq_message_detail})
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    current_status=db.query(Status).filter(Status.id == str(new_order_header.order_status_uuid)).first()
    response.headers["Location"] = str(new_order_header.id)
    ei.send_document(new_order_header,'orders','create')
    rmq.send_message(rmq_message,'orders','create',current_status.key_name.lower())
    rc.register_data('order_' + str(new_order_header.id),'create', rmq_message)
    return {'message:': 'Order created'}

@router.delete('/orders/header/{order_header_uuid}',
               tags=["Orders"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete_header(response:Response, order_header_uuid:str):
      try:
        to_delete=db.query(Order_Header).filter(Order_Header.id == order_header_uuid).first()
        if to_delete is None:
          return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_delete.active=False
        to_delete.modified_at=get_current_date()
        db.commit()
        db.query(Order_Detail).filter(Order_Detail.order_header_uuid == order_header_uuid).update({Order_Detail.active: False})
        db.commit()
      except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
      response.headers["Location"] = str(to_delete.id)
      ei.send_document(to_delete,'orders','delete')
      rmq.send_message(to_delete.to_json(),'orders','delete')
      rc.register_data(str(to_delete.id),'update', to_delete.to_json())
      return {'message:': 'Order deleted'}

@router.delete('/orders/header/{order_header_uuid}/detail/{product_uuid}',
               tags=["Orders"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete_detail(response:Response, order_header_uuid:str,product_uuid:str):
      try:
        to_delete=db.query(Order_Detail).filter(and_(Order_Detail.order_header_uuid == order_header_uuid, Order_Detail.product_uuid == product_uuid)).first()
        if to_delete is None:
          return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_delete.active=False
        to_delete.modified_at=get_current_date()
        db.commit()
      except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
      response.headers["Location"] = str(to_delete.id)
      ei.send_document(to_delete,'orders','delete')
      rmq.send_message(to_delete.to_json(),'orders','delete')
      rc.register_data(str(to_delete.id),'update', to_delete.to_json())
      return {'message:': 'Order deleted'}

@router.put('/orders/header/{order_header_uuid}/num_ticket',
            tags=["Orders"],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def update_num_ticket(response:Response, order_header_uuid:str, config: Config_Request):
    valid_uuid=Check_uuid.check_uuid(order_header_uuid)
    try:
        to_update=db.query(Order_Header).filter(Order_Header.id==valid_uuid).first()
        if to_update is  None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Order Not Found")
        to_update.num_ticket=config.counter
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'orders','update')
    rmq.send_message(to_update.to_json(),'orders','update','num_tiket')
    rc.register_data(str(to_update.id),'update', to_update.to_json())
    return {'message:': 'Num_ticket assigned'}

@router.put('/orders/header/{order_header_uuid}/status',
            tags=["Orders"],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def update_status(response:Response, order_header_uuid:str, status_body:Status_Request):
    valid_uuid=Check_uuid.check_uuid(order_header_uuid)
    try:
        to_update=db.query(Order_Header).filter(Order_Header.id==valid_uuid).first()
        if to_update is  None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_update.order_status_uuid =str(status_body.id)
        to_update.modified_at=get_current_date()
        db.commit()
        current_status=db.query(Status).filter(Status.id == str(status_body.id)).first()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'orders','update')
    rmq.send_message(to_update.to_json(),'orders','update',current_status.key_name)
    return {'message:': 'Order Status modified'}

@router.post('/orders/demand/{order_header_uuid}',
             tags=["Orders"],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def order_pdf(response:Response, order_header_uuid:str):
    try:
      result = order_demand_service.get_pdf(order_header_uuid)
      return result
    except Exception as e:
      response.status_code = status.HTTP_400_BAD_REQUEST
      return {'message:': e}




