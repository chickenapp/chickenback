import os
from fastapi import APIRouter
from database import SessionLocal

router = APIRouter()

db= SessionLocal()

@router.get('/alive',tags=["Alive"],summary="Check status service",
    description="Check if backend is up",
)
async def alive():
    if str(os.getenv('DEV_MODE')) == 'true':
        return {"message":"Backend is ready and alive in DEBUG MODE"}
    else:
        return {"message":"Backend is ready and alive in PRODUCTION"}
