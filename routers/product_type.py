
from fastapi import APIRouter, Depends,status,HTTPException, Response
from fastapi_pagination import Page, paginate
from utils.dates import get_current_date
from utils.security import verify_key
from utils.rmq_publish import RabbitMQ as rmq
from utils.check_uuid import Check_uuid
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.product_type_model import Product_Type
from serializers.product_type_serializer import *
from utils.elastic_search import Elastic_Index as ei
from config.redis import Redis_Cluster as rc
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/product_type',
            tags=["Products_Type"],
            response_model=Page[Product_Type_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)] )
def get_all(response:Response):
    product_type=db.query(Product_Type).all()
    return paginate(product_type)

@router.get('/product_type/{product_type_uuid}',
            tags=["Products_Type"],
            response_model=Product_Type_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(product_type_uuid:str):
    valid_uuid=Check_uuid.check_uuid(product_type_uuid)
    product_type=db.query(Product_Type).filter(Product_Type.id==valid_uuid).first()
    return product_type

@router.post('/product_type/name',
             tags=["Products_Type"],
             response_model=Page[Product_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_name(product_type:Product_Type_Search):
    product_type=db.query(Product_Type).filter(Product_Type.name.contains(product_type.name)).all()
    return paginate(product_type)

@router.post('/product_type/actives',
             tags=["Products_Type"],
             response_model=Page[Product_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_actives(response:Response):
    product_type=db.query(Product_Type).filter(Product_Type.active == True).all()
    if product_type == None:
        return {'message:': 'No Product Type found'}
    return paginate(product_type)

@router.post('/product_type/inactives',
             tags=["Products_Type"],
             response_model=Page[Product_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_inactives(response:Response):
    product_type=db.query(Product_Type).filter(Product_Type.active == False).all()
    if product_type == None:
        return {'message:': 'No Product Type found'}
    return paginate(product_type)

@router.post('/product_type',
             tags=["Products_Type"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, product_type:Product_Type_Request):
    try:
        new_product_type=Product_Type(
            id=Generate_Uuid.generate_new_uuid(),
            name=product_type.name,
            description=product_type.description,
            isStock=product_type.isStock,
            active=product_type.active
        )
        db.add(new_product_type)
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(new_product_type.id)
    ei.send_document(new_product_type,'products_type','create')
    rmq.send_message(new_product_type.to_json(),'products_type','create')
    rc.register_data('product_type_' + str(new_product_type.id), 'create', new_product_type.to_json())
    return {'message:': 'Product Type created'}

@router.put('/product_type/{product_type_uuid}',
            tags=["Products_Type"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, product_type_uuid:str, product_type:Product_Type_Request):
    valid_uuid=Check_uuid.check_uuid(product_type_uuid)
    try:
        to_update=db.query(Product_Type).filter(Product_Type.id==valid_uuid).first()
        if to_update is  None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_update.name =product_type.name
        to_update.isStock=product_type.isStock
        to_update.active=product_type.active
        to_update.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'products_type','update')
    rmq.send_message(to_update.to_json(),'products_type','update')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Product Type modified'}

@router.delete('/product_type/{product_type_uuid}',
               tags=["Products_Type"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete(response:Response,product_type_uuid:str):
    valid_uuid=Check_uuid.check_uuid(product_type_uuid)
    try:
        to_delete=db.query(Product_Type).filter(Product_Type.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Type Not Found")
        to_delete.active = False
        to_delete.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'products_type','delete')
    rmq.send_message(to_delete.to_json(),'products_type','delete')
    rc.register_data(str(to_delete.id), 'update', to_delete.to_json())
    return {'message:': 'Product Type deleted'}