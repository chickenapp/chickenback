from fastapi import APIRouter, Depends, UploadFile, File,status,HTTPException, Response
from fastapi_pagination import Page, paginate
from utils.dates import get_current_date
from utils.security import verify_key
from utils import images
from utils.check_uuid import Check_uuid
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.product_model import Product
from models.unit_measure_model import Unit_Measure
from models.product_type_model import Product_Type
from serializers.product_serializer import *
from serializers.product_type_serializer import *
from utils.elastic_search import Elastic_Index as ei
from utils.rmq_publish import RabbitMQ as rmq
from config.redis import Redis_Cluster as rc
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/products',
            tags=["Products"],
            response_model=Page[Product_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_all(response:Response):
    products=db.query(Product).join(Unit_Measure).join(Product_Type).with_entities(Product.id,
                                                                            Product.product_image,
                                                                            Product.name,
                                                                            Product.description,
                                                                            Product.quantity,
                                                                            Product.price,
                                                                            Product.warning_stock,
                                                                            Product.product_type_uuid,
                                                                            Product.active,
                                                                            Product.created_at,
                                                                            Unit_Measure.name.label('um_name'),
                                                                            Product_Type.name.label('product_type_name')
                                                                            ).all()
    response.headers["custom_header"] = "J9rlBf4zhm7h9pMdg5ASpRho7axu6WaUMum0n6bB"
    return paginate(products)

@router.get('/products/{product_uuid}',
            tags=["Products"],
            response_model=Product_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(product_uuid:str):
    valid_uuid=Check_uuid.check_uuid(product_uuid)
    product=db.query(Product).join(Unit_Measure).join(Product_Type).with_entities(Product.id,
                                                                            Product.product_image,
                                                                            Product.name,
                                                                            Product.description,
                                                                            Product.quantity,
                                                                            Product.price,
                                                                            Product.warning_stock,
                                                                            Product.product_type_uuid,
                                                                            Product.active,
                                                                            Product.created_at,
                                                                            Unit_Measure.name.label('um_name'),
                                                                            Product_Type.name.label('product_type_name')
                                                                            ).filter(Product.id==valid_uuid).first()

    if not product:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product not exist")
    return product

@router.post('/products/product_type',
             tags=["Products"],
             response_model=Page[Product_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_type(product:Product_Type_Search):
    products=db.query(Product).join(Unit_Measure).join(Product_Type).with_entities(Product.id,
                                                                            Product.product_image,
                                                                            Product.name,
                                                                            Product.description,
                                                                            Product.quantity,
                                                                            Product.price,
                                                                            Product.warning_stock,
                                                                            Product.product_type_uuid,
                                                                            Product.active,
                                                                            Product.created_at,
                                                                            Unit_Measure.name.label('um_name'),
                                                                            Product_Type.name.label('product_type_name')
                                                                            ).filter(Product.product_type_uuid==str(product.id)
                                                                            ).all()
    return paginate(products)

@router.post('/products/actives',
             tags=["Products"],
             response_model=Page[Product_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_actives(response:Response):
    product=db.query(Product).join(Unit_Measure).join(Product_Type).with_entities(Product.id,
                                                                            Product.product_image,
                                                                            Product.name,
                                                                            Product.description,
                                                                            Product.quantity,
                                                                            Product.price,
                                                                            Product.warning_stock,
                                                                            Product.product_type_uuid,
                                                                            Product.active,
                                                                            Product.created_at,
                                                                            Unit_Measure.name.label('um_name'),
                                                                            Product_Type.name.label('product_type_name')
                                                                            ).filter(Product.active == True).all()
    if product== None:
        return {'message:': 'No Product found'}
    return paginate(product)

@router.post('/products/inactives',
             tags=["Products"],
             response_model=Page[Product_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_inactives(response:Response):
    product=db.query(Product).join(Unit_Measure).join(Product_Type).with_entities(Product.id,
                                                                            Product.product_image,
                                                                            Product.name,
                                                                            Product.description,
                                                                            Product.quantity,
                                                                            Product.price,
                                                                            Product.warning_stock,
                                                                            Product.product_type_uuid,
                                                                            Product.active,
                                                                            Product.created_at,
                                                                            Unit_Measure.name.label('um_name'),
                                                                            Product_Type.name.label('product_type_name')
                                                                            ).filter(Product.active == False).all()
    if product== None:
            return {'message:': 'No Product found'}
    return paginate(product)

@router.post('/products',
             tags=["Products"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, product:Product_Request):
    try:
        new_product=Product(
            id=Generate_Uuid.generate_new_uuid(),
            product_image=product.product_image or images.Images.get_default_image(),
            name=product.name,
            description=product.description,
            price=product.price,
            quantity=product.quantity,
            warning_stock=product.warning_stock,
            um_uuid=product.um_uuid,
            product_type_uuid=product.product_type_uuid,
            active=product.active
            )
        db.add(new_product)
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(new_product.id)
    ei.send_document(new_product,'products','create')
    rmq.send_message(new_product,'products','create')
    rc.register_data('product_' + str(new_product.id), 'update', new_product.to_json())
    return {'message:': 'Product created'}

@router.post('/products/upload-file/{product_uuid}',
             tags=["Products"],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def upload_file(response:Response, product_uuid:str, uploaded_file: UploadFile = File(...)):
    valid_uuid=Check_uuid.check_uuid(product_uuid)
    try:
        file_location= images.Images.get_b64_image(uploaded_file)
        to_update=db.query(Product).filter(Product.id==valid_uuid).first()
        to_update.product_image=file_location
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    ei.send_document(to_update,'products','uploadfile')
    rmq.send_message(to_update.to_json(),'products','uploadfile')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Image uploaded'}

@router.put('/products/{product_uuid}',
            tags=["Products"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, product_uuid:str, product:Product_Request):
    valid_uuid=Check_uuid.check_uuid(product_uuid)
    try:
        to_update=db.query(Product).filter(Product.id==valid_uuid).first()
        if to_update is  None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_update.product_image=product.product_image
        to_update.name=product.name
        to_update.description=product.description
        to_update.price=product.price
        to_update.quantity=product.quantity
        to_update.warning_stock=product.warning_stock
        to_update.um_uuid=product.um_uuid
        to_update.product_type_uuid=product.product_type_uuid
        to_update.active=product.active
        to_update.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'products','update')
    rmq.send_message(to_update.to_json(),'products','update')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Product modified'}

@router.delete('/products/{product_uuid}',
               tags=["Products"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete(response:Response,product_uuid:str):
    valid_uuid=Check_uuid.check_uuid(product_uuid)
    try:
        to_delete=db.query(Product).filter(Product.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_delete.active = False
        to_delete.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'products','delete')
    rmq.send_message(to_delete.to_json(),'products','delete')
    rc.register_data(str(to_delete.id), 'update', to_delete.to_json())
    return {'message:': 'Product Type deleted'}