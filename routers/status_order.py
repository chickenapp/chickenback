from fastapi import APIRouter, Depends, HTTPException, status, Response
from fastapi_pagination import Page,  paginate
from utils.dates import get_current_date
from utils.security import verify_key
from utils.rmq_publish import RabbitMQ as rmq
from utils.check_uuid import Check_uuid
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.status_model import Status
from serializers.status_serializer import *
from utils.elastic_search import Elastic_Index as ei
from config.redis import Redis_Cluster as rc
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/status',
            tags=["Status"],
            response_model=Page[Status_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_all(response:Response):
    status=db.query(Status).all()
    return paginate(status)

@router.get('/status/{status_uuid}', tags=["Status"],
            response_model=Status_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(status_uuid:str):
    valid_uuid=Check_uuid.check_uuid(status_uuid)
    status=db.query(Status).filter(Status.id==valid_uuid).first()
    return status

@router.post('/status/search',
             tags=["Status"],
             response_model=Page[Status_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_name(status:Status_Search):
    status=db.query(Status).filter(Status.name.contains(status.name)).all()
    return paginate(status)

@router.post('/status/actives',
             tags=["Status"],
             response_model=Page[Status_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_actives(response:Response):
    status=db.query(Status).filter(Status.active == True).all()
    if status == None:
        return {'message:': 'No Status found'}
    return paginate(status)

@router.post('/status/inactives',
             tags=["Status"],
             response_model=Page[Status_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_inactives(response:Response):
    status=db.query(Status).filter(Status.active == False).all()
    if status == None:
        return {'message:': 'No Status found'}
    return paginate(status)

@router.post('/status/default',
             tags=["Status"],
             response_model=Status_Response,
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_default(response:Response):
    status=db.query(Status).filter(Status.isDefault == True).first()
    if status == None:
        return {'message:': 'No Status found'}
    response.headers["Location"] = str(status.id)
    return status

@router.post('/status',
             tags=["Status"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, status:Status_Request):
    try:
        new_status=Status(
        id=Generate_Uuid.generate_new_uuid(),
        name=status.name,
        active=status.active
        )
        db.add(new_status)
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(new_status.id)
    ei.send_document(new_status,'status','create')
    rmq.send_message(new_status.to_json(),'status','create')
    rc.register_data('status_' + str(new_status.id), 'create', new_status.to_json())
    return {'message:': 'Status created'}

@router.put('/status/{status_uuid}',
            tags=["Status"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, status_uuid:str, status:Status_Request):
    valid_uuid=Check_uuid.check_uuid(status_uuid)
    try:
        to_update=db.query(Status).filter(Status.id==valid_uuid).first()
        if to_update is  None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Status Not Found")
        to_update.name =status.name
        to_update.active=status.active
        to_update.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'status','update')
    rmq.send_message(to_update.to_json(),'status','update')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Status modified'}

@router.delete('/status/{status_uuid}',
               tags=["Status"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete(response:Response, status_uuid:str):
    valid_uuid=Check_uuid.check_uuid(status_uuid)
    try:
        to_delete=db.query(Status).filter(Status.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Status Not Found")
        to_delete.active = False
        to_delete.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'status','delete')
    rmq.send_message(to_delete.to_json(),'status','delete')
    rc.register_data(str(to_delete.id), 'update', to_delete.to_json())
    return {'message:': 'Status deleted'}