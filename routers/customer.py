from typing import List
from fastapi import APIRouter, Depends,status,HTTPException, Response
from fastapi_pagination import Page, add_pagination, paginate
from utils.security import verify_key
from utils.check_uuid import Check_uuid
from sqlalchemy import func, or_
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.customer_model import Customer
from serializers.customer_serializer import *
from utils.elastic_search import Elastic_Index as ei
from utils.rmq_publish import RabbitMQ as rmq
from config.log import  logging
from utils.dates import *
from config.redis import Redis_Cluster as rc

router = APIRouter()

db= SessionLocal()

@router.get('/customers',
            tags=["Customer"],
            response_model=Page[Customer_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_all(response:Response):
    customers=db.query(Customer).with_entities( Customer.id,
                                                Customer.name,
                                                Customer.phone,
                                                Customer.email,
                                                Customer.created_at,
                                                Customer.active
                                                ).order_by(Customer.name.asc()).all()
    response.headers["location"] = "J9rlBf4zhm7h9pMdg5ASpRho7axu6WaUMum0n6bB"
    return paginate(customers)

@router.get('/customers/schedule',
            tags=["Customer"],
            response_model=List[Customer_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_schedule():
    customers=db.query(Customer).with_entities( Customer.id,
                                                Customer.name,
                                                Customer.phone,
                                                Customer.email,
                                                Customer.created_at,
                                                Customer.active
                                                ).order_by(Customer.name.asc()).all()
    return customers

@router.get('/customers/{customer_uuid}',
            tags=["Customer"],
            response_model=Customer_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(customer_uuid:str):
    valid_uuid=Check_uuid.check_uuid(customer_uuid)
    customer=db.query(Customer).with_entities(  Customer.id,
                                                Customer.name,
                                                Customer.phone,
                                                Customer.email,
                                                Customer.created_at,
                                                Customer.active
                                                    ).filter(Customer.id==valid_uuid).first()
    if not customer:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Customer not exist")
    return customer

@router.post('/customers/search',
             tags=["Customer"],
             response_model=Page[Customer_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_param(customer_seach:Customer_Search):
    customer = db.query(Customer)
    if customer_seach.name:
        customer = customer.filter(func.lower(Customer.name).contains(customer_seach.name.lower()))
    if customer_seach.email:
        customer = customer.filter(func.lower(Customer.email).contains(customer_seach.email.lower()))
    if customer_seach.phone:
         customer = customer.filter(Customer.phone.contains(customer_seach.phone))
    return paginate(customer.all())

@router.post('/customers/actives',
             tags=["Customer"],
             response_model=Page[Customer_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_actives(response:Response):
    customer=db.query(Customer).filter(Customer.active == True).all()
    if customer == None:
        return {'message:': 'No Customer found'}
    return paginate(customer)

@router.post('/customers/inactives',
             tags=["Customer"],
             response_model=Page[Customer_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_inactives(response:Response):
    customer=db.query(Customer).filter(Customer.active == False).all()
    if customer == None:
        return {'message:': 'No Customer found'}
    return paginate(customer)

@router.post('/customers',
             tags=["Customer"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, customer:Customer_Request):
    try:
        new_customer=Customer(
        id=Generate_Uuid.generate_new_uuid(),
        name=customer.name,
        phone=customer.phone,
        email=customer.email,
        active=customer.active
        )
        db.add(new_customer)
        db.commit()
        response.headers["Location"] = str(new_customer.id)
        ei.send_document(new_customer,'customers','create')
        rmq.send_message(new_customer.to_json(),'customers','create')
        rc.register_data('customer_'+ str(new_customer.id), 'create', new_customer.to_json())
        return {'message:': 'Customer created'}
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}

@router.put('/customers/{customer_uuid}',
            tags=["Customer"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, customer_uuid:str, customer:Customer_Request):
    valid_uuid=Check_uuid.check_uuid(customer_uuid)
    try:
        to_update=db.query(Customer).filter(Customer.id==valid_uuid).first()
        if to_update is  None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Customer Not Found")
        to_update.name=customer.name
        to_update.email=customer.email
        to_update.phone=customer.phone
        to_update.active=customer.active
        to_update.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}

    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'customers','update')
    rmq.send_message(to_update.to_json(),'customers','update')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Customer modified'}

@router.delete('/customers/{customer_uuid}',
               tags=["Customer"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete(response:Response, customer_uuid:str):
    valid_uuid=Check_uuid.check_uuid(customer_uuid)
    try:
        to_delete=db.query(Customer).filter(Customer.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Customer Not Found")
        to_delete.modified_at=get_current_date()
        to_delete.active = False
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'customers','delete')
    rmq.send_message(to_delete.to_json(),'customers','delete')
    rc.register_data(str(to_delete.id),'update', to_delete.to_json())
    return {'message:': 'Customer deleted'}