from datetime import timedelta
from datetime import datetime
from operator import and_
import os
from urllib.parse import uses_relative
from fastapi import APIRouter, Depends,status,HTTPException, Response
from utils.security import verify_key
from database import SessionLocal
from models.user_model import Role, User
from serializers.auth_serializer import Auth_User_Request, Auth_User_Response, Token_Response
from serializers.customer_serializer import *
from utils.elastic_search import Elastic_Index as ei
import jwt
from dotenv import load_dotenv

load_dotenv()

router = APIRouter()

db= SessionLocal()

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, os.getenv('SECRET_KEY'), algorithm=os.getenv('ALGORITHM'))
    return {'token':encoded_jwt, 'refresh_token':'','expire_time': expire }


@router.post('/login/token',
            tags=["Login"],
            response_model=Token_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def auth_token(response:Response, user_data:Auth_User_Request):
    user=db.query(User).filter(and_(User.email == user_data.email, User.password == user_data.password)).first()
    if user is None:
        return HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials")
    roles=db.query(Role).filter(Role.users.any(User.id == user.id)).all()
    roles_uuid = []
    for rol in roles:
        roles_uuid.append({rol.name, str(rol.id)})
    data={}
    response_jwt= create_access_token(data)
    response_jwt.update({'user_uuid': str(user.id), 'roles_active': roles_uuid})
    # response.headers["location"] = ""
    return response_jwt


