from fastapi import APIRouter, Depends, UploadFile, File,status,HTTPException, Response
from fastapi_pagination import Page, paginate
from utils.dates import get_current_date
from utils.security import verify_key
from utils import rmq_publish
from utils.check_uuid import Check_uuid
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.unit_measure_model import Unit_Measure
from serializers.unit_measure_serializer import *
from utils.elastic_search import Elastic_Index as ei
from utils.rmq_publish import RabbitMQ as rmq
from config.redis import Redis_Cluster as rc
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/units_measure', tags=["Units of Measure"],
            response_model=Page[Unit_Measure_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_all(response:Response):
    um=db.query(Unit_Measure).with_entities( Unit_Measure.id,
                                                    Unit_Measure.name,
                                                    Unit_Measure.convert_factor,
                                                    Unit_Measure.created_at,
                                                    Unit_Measure.active
                                                    ).all()
    response.headers["location"] = "J9rlBf4zhm7h9pMdg5ASpRho7axu6WaUMum0n6bB"
    return paginate(um)

@router.get('/units_measure/{unit_measure_uuid}', tags=["Units of Measure"],
            response_model=Unit_Measure_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(unit_measure_uuid: str):
    valid_uuid=Check_uuid.check_uuid(unit_measure_uuid)
    um=db.query(Unit_Measure).with_entities( Unit_Measure.id,
                                                    Unit_Measure.name,
                                                    Unit_Measure.convert_factor,
                                                    Unit_Measure.created_at,
                                                    Unit_Measure.active
                                                    ).filter(Unit_Measure.id==valid_uuid).first()
    if not um:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Unit Measure not exist")
    return um

@router.post('/units_measure/search',
             tags=["Units of Measure"],
             response_model=Page[Unit_Measure_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_name(unit_measure: Unit_Measure_Search):
    um=db.query(Unit_Measure).filter(Unit_Measure.name.contains(unit_measure.name)).all()
    return paginate(um)

@router.post('/units_measure/actives',
             tags=["Units of Measure"],
             response_model=Page[Unit_Measure_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_actives(response:Response):
    um=db.query(Unit_Measure).filter(Unit_Measure.active == True).all()
    if um == None:
        return {'message:': 'No Status found'}
    return paginate(um)

@router.post('/units_measure/inactives',
             tags=["Units of Measure"],
             response_model=Page[Unit_Measure_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_all_inactives(response:Response):
    um=db.query(Unit_Measure).filter(Unit_Measure.active == False).all()
    if um == None:
        return {'message:': 'No Units Measure found'}
    return paginate(um)

@router.post('/units_measure',
             tags=["Units of Measure"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, unit_measure: Unit_Measure_Request):
    try:
        new_um=Unit_Measure(
        id=Generate_Uuid.generate_new_uuid(),
        name=unit_measure.name,
        active=unit_measure.active
        )
        db.add(new_um)
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(new_um.id)
    ei.send_document(new_um,'units_measure','create')
    rmq.send_message(new_um.to_json(),'units_measure','create')
    rc.register_data(str(new_um.id), 'update', new_um.to_json())
    return {'message:': 'Unit Measure created'}

@router.put('/units_measure/{unit_measure_uuid}',
            tags=["Units of Measure"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, unit_measure_uuid: str, unit_measure:Unit_Measure_Request):
    valid_uuid=Check_uuid.check_uuid(unit_measure_uuid)
    to_update=db.query(Unit_Measure).filter(Unit_Measure.id==valid_uuid).first()
    if to_update is  None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Unit Measure Not Found")
    to_update.name=unit_measure.name
    to_update.active=unit_measure.active
    to_update.convert_factor=unit_measure.convert_factor
    to_update.modified_at=get_current_date()
    db.commit()
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'units_measure','update')
    rmq.send_message(to_update.to_json(),'units_measure','update')
    rc.register_data(str(to_update.id), 'update', to_update.to_json())
    return {'message:': 'Unit Measure modified'}

@router.delete('/units_measure/{unit_measure_uuid}',
               tags=["Units of Measure"],
               status_code=status.HTTP_200_OK ,
               dependencies=[Depends(verify_key)])
def delete(response:Response, unit_measure_uuid:str):
    valid_uuid=Check_uuid.check_uuid(unit_measure_uuid)
    try:
        to_delete=db.query(Unit_Measure).filter(Unit_Measure.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Unit Measure Not Found")
        to_delete.active = False
        to_delete.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'units_measure','delete')
    rmq.send_message(to_delete.to_json(),'units_measure','delete')
    rc.register_data(str(to_delete.id), 'update', to_delete.to_json())
    return {'message:': 'Unit Measure deleted'}