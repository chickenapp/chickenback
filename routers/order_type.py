from fastapi import APIRouter, Depends,status,HTTPException, Response
from fastapi_pagination import Page, paginate
from utils.dates import get_current_date
from utils.security import verify_key
from utils.check_uuid import Check_uuid
from database import SessionLocal
from utils.gen_uuid4 import Generate_Uuid
from models.order_type_model import Order_Type
from serializers.order_type_serializer import Order_Type_Request, Order_Type_Response, Order_Type_Search
from serializers.product_type_serializer import *
from utils.elastic_search import Elastic_Index as ei
from utils.rmq_publish import RabbitMQ as rmq
from config.redis import Redis_Cluster as rc
from config.log import logging

router = APIRouter()

db= SessionLocal()

@router.get('/order_type',
            tags=["Order_Type"],
            response_model=Page[Order_Type_Response],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)] )
def get_all(response:Response):
    order_type=db.query(Order_Type).all()
    return paginate(order_type)

@router.get('/order_type/{order_type_uuid}',
            tags=["Order_Type"],
            response_model=Order_Type_Response,
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def get_by_id(order_type_uuid:str):
    valid_uuid=Check_uuid.check_uuid(order_type_uuid)
    order_type=db.query(Order_Type).filter(Order_Type.id==valid_uuid).first()
    if order_type == None:
        return {'message:': 'Order Type not found'}
    return order_type

@router.post('/order_type/actives',
             tags=["Order_Type"],
             response_model=Page[Order_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_actives(response:Response):
    order_type=db.query(Order_Type).filter(Order_Type.active == True).all()
    if order_type == None:
        return {'message:': 'No Order Type'}
    return paginate(order_type)

@router.post('/order_type/inactives',
             tags=["Order_Type"],
             response_model=Page[Order_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)] )
def get_all_inactives(response:Response):
    order_type=db.query(Order_Type).filter(Order_Type.active == False).all()
    if order_type == None:
        return {'message:': 'No Order Type found'}
    return paginate(order_type)

@router.post('/order_type/default',
             tags=["Status"],
             response_model=Order_Type_Response,
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def get_default(response:Response):
    order_type=db.query(Order_Type).filter(Order_Type.isDefault == True).first()
    if order_type == None:
        return {'message:': 'No Order Type found'}
    response.headers["Location"] = str(order_type.id)
    return order_type

@router.post('/order_type/name',
             tags=["Order_Type"],
             response_model=Page[Order_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_name(order_type:Order_Type_Search):
    order_type=db.query(Order_Type).filter(Order_Type.name.contains(order_type.name)).all()
    if order_type == None:
        return {'message:': 'No Order Type found'}
    return paginate(order_type)

@router.post('/order_type/description',
             tags=["Order_Type"],
             response_model=Page[Order_Type_Response],
             status_code=status.HTTP_200_OK,
             dependencies=[Depends(verify_key)])
def find_by_name(order_type:Order_Type_Search):
    order_type=db.query(Order_Type).filter(Order_Type.name.contains(order_type.name)).all()
    if order_type == None:
        return {'message:': 'No Order Type found'}
    return paginate(order_type)

@router.post('/order_type',
             tags=["Order_Type"],
             status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(verify_key)])
def create(response:Response, order_type:Product_Type_Request):
    try:
        new_order_type=Order_Type(
            id=Generate_Uuid.generate_new_uuid(),
            name=order_type.name,
            description=order_type.description
        )
        db.add(new_order_type)
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(new_order_type.id)
    ei.send_document(new_order_type,'orders_type','create')
    rmq.send_message(new_order_type.to_json(),'orders_type','create')
    rc.register_data('order_type_'+ str(new_order_type.id),'create', new_order_type.to_json())
    return {'message:': 'Order Type created'}

@router.put('/order_type/{order_type_uuid}',
            tags=["Order_Type"],
            status_code=status.HTTP_200_OK,
            dependencies=[Depends(verify_key)])
def update(response:Response, order_type_uuid:str, order_type:Order_Type_Request):
    valid_uuid=Check_uuid.check_uuid(order_type_uuid)
    try:
        to_update=db.query(Order_Type).filter(Order_Type.id==valid_uuid).first()
        if to_update is  None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Not Found")
        to_update.name=order_type.name
        to_update.description=order_type.description
        to_update.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_update.id)
    ei.send_document(to_update,'orders_type','update')
    rmq.send_message(to_update.to_json(),'orders_type','update')
    rc.register_data(str(to_update.id),'update', to_update.to_json())
    return {'message:': 'Order Type modified'}

@router.delete('/order_type/{order_type_uuid}',
               tags=["Order_Type"],
               status_code=status.HTTP_200_OK,
               dependencies=[Depends(verify_key)])
def delete(response:Response,order_type_uuid:str):
    valid_uuid=Check_uuid.check_uuid(order_type_uuid)
    try:
        to_delete=db.query(Order_Type).filter(Order_Type.id==valid_uuid).first()
        if to_delete is None:
            return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Product Type Not Found")
        to_delete.active = False
        to_delete.modified_at=get_current_date()
        db.commit()
    except Exception as e:
        db.rollback()
        logging.error(e)
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'message:': e}
    response.headers["Location"] = str(to_delete.id)
    ei.send_document(to_delete,'orders_type','delete')
    rmq.send_message(to_delete.to_json(),'orders_type','delete')
    rc.register_data(str(to_delete.id),'update', to_delete.to_json())
    return {'message:': 'Order Type deleted'}