import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import String, Column, Text, Float,Integer, Boolean, DateTime, ForeignKey
from sqlalchemy_utils import UUIDType
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.inspection import inspect
from dotenv import load_dotenv

load_dotenv()

class Product(Base):
    __tablename__='products'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    product_image=Column(Text,nullable=True)
    name=Column(String(255),nullable=True)
    description=Column(String(255),nullable=True)
    price=Column(Float, default=0)
    quantity=Column(Integer,default=0)
    warning_stock=Column(Float,default=0)
    um_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.units_measure.id')) #
    product_type_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.products_type.id')) #
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)
    order_detail=relationship('Order_Detail')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

    def to_json(self):
            return {
        'id': str(self.id),
        'product_image': str(self.product_image),
        'name':str(self.name),
        'description': self.description,
        'price':self.price,
        'quantity': self.quantity,
        'warning_stock': self.warning_stock,
        'um_uuid': str(self.um_uuid),
        'product_type_uuid':str(self.product_type_uuid),
        'created_at':str(self.created_at),
        'modified_at':str(self.created_at) or None,
        'active': self.active,
    }