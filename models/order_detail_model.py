import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy import Column, Float,Integer, Boolean, DateTime, ForeignKey
from sqlalchemy_utils import UUIDType
from dotenv import load_dotenv

load_dotenv()

class Order_Detail(Base):
    __tablename__='order_details'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    order_header_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.order_headers.id', ondelete='CASCADE')) #
    product_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.products.id', ondelete='CASCADE')) #
    price=Column(Float, default=0)
    quantity=Column(Integer, default=0)
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

    def to_json(self):
        return {
        'id': str(self.id),
        'order_header_uuid': str(self.order_header_uuid),
        'product_uuid':str(self.product_uuid),
        'price': self.price,
        'quantity':self.quantity,
        'created_at':str(self.created_at),
        'modified_at':str(self.created_at) or None,
        'active': self.active,
    }