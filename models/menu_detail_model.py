from datetime import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy import  Boolean, Column, DateTime, ForeignKey, Integer
from sqlalchemy_utils import UUIDType
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.inspection import inspect
from dotenv import load_dotenv

load_dotenv()

class Menu_Detail(Base):
    __tablename__='menu_details'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    menu_header_uuid=Column(UUIDType(binary=False), ForeignKey('menu_headers.id')) #
    product_uuid=Column(UUIDType(binary=False), ForeignKey('products.id')) #
    isOptional=Column(Boolean,default=False)
    quantity=Column(Integer, default=0)
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}
