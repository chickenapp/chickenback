import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import String, Column, Text, Float,Integer, Boolean, DateTime, ForeignKey
from sqlalchemy_utils import UUIDType
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.inspection import inspect
from dotenv import load_dotenv

load_dotenv()

class Order_Header(Base):
    __tablename__='order_headers'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    entry_date=Column(DateTime)
    delivery_date=Column(DateTime)
    customer_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.customers.id')) #
    order_type_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.orders_type.id')) #
    order_status_uuid=Column(UUIDType(binary=False), ForeignKey(os.getenv('DB_SCHEMA') + '.status.id')) #
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    total=Column(Float, default=0)
    num_ticket=Column(Integer, default=0)
    active=Column(Boolean,default=True)
    order_detail=relationship('Order_Detail')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

    def to_json(self):
        return {
            'id': str(self.id),
            'entry_date': str(self.entry_date),
            'delivery_date':str(self.delivery_date),
            'customer_uuid': str(self.customer_uuid),
            'order_type_uuid':str(self.order_type_uuid),
            'order_status_uuid': str(self.order_status_uuid),
            'created_at':str(self.created_at),
            'modified_at':str(self.created_at) or None,
            'total': self.total,
            'num_ticket': self.num_ticket,
            'active': self.active,
        }