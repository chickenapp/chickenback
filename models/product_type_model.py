import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import String, Column, Text, Float,Integer, Boolean, DateTime, ForeignKey
from sqlalchemy_utils import UUIDType
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.inspection import inspect
from dotenv import load_dotenv

load_dotenv()

class Product_Type(Base, SerializerMixin):
    __tablename__='products_type'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    name=Column(String(255),nullable=True)
    description=Column(String(255),nullable=True)
    isStock=Column(Boolean,default=True)
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)
    product=relationship('Product')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

    def to_json(self):
        return {
            'id': str(self.id),
            'name':str(self.name),
            'description': self.description,
            'isStock':self.isStock,
            'created_at':str(self.created_at),
            'modified_at':str(self.created_at) or None,
            'active': self.active,
        }