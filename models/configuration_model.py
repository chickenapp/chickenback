import os
from uuid import uuid4
from database import Base
from sqlalchemy import Integer, String, Column,  Boolean
from sqlalchemy_utils import UUIDType
from dotenv import load_dotenv

load_dotenv()

class Config(Base):
    __tablename__='configurations'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    ip_local_printer=Column(String(255),nullable=True)
    num_ticket=Column(Integer, default=0)
    active=Column(Boolean,default=True)

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}