import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import String, Column,  Boolean, DateTime, ForeignKey, Table
from sqlalchemy_utils import UUIDType
from dotenv import load_dotenv

load_dotenv()

user_rol = Table('users_roles', Base.metadata,
                 Column('user_id',
                        ForeignKey(os.getenv('DB_SCHEMA') + '.users.id'),primary_key=True),
                 Column('role_id',
                        ForeignKey(os.getenv('DB_SCHEMA') + '.roles.id'),primary_key=True),
                schema=os.getenv('DB_SCHEMA')
                )

class User(Base):
    __tablename__='users'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    name=Column(String(255),nullable=False)
    password=Column(String(255),nullable=False)
    email=Column(String(255),nullable=True, unique=True)
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)
    roles = relationship('Role', secondary=user_rol, back_populates='users')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

class Role(Base):
    __tablename__='roles'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    name=Column(String(255),nullable=False)
    users = relationship('User', secondary=user_rol, back_populates='roles')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}