import datetime
import os
from uuid import uuid4
from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import String, Column, Text, Float,Integer, Boolean, DateTime, ForeignKey
from sqlalchemy_utils import UUIDType
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.inspection import inspect
from dotenv import load_dotenv

load_dotenv()

class Menu_Header(Base):
    __tablename__='menu_headers'
    __table_args__ = {'schema': os.getenv('DB_SCHEMA')}

    id=Column(UUIDType(binary=False), default=uuid4, primary_key=True)
    name=Column(String(255),nullable=False)
    price=Column(Float, default=0)
    date_from=Column(DateTime, default=datetime.datetime.utcnow)
    date_to=Column(DateTime, default=datetime.datetime.utcnow + datetime.timedelta(days=30))
    created_at=Column(DateTime, default=datetime.datetime.utcnow)
    modified_at=Column(DateTime)
    active=Column(Boolean,default=True)
    menu_detail=relationship('Menu_Detail')

    def serialize(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}
