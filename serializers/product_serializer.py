from datetime import datetime
from typing import Optional
from pydantic import BaseModel
from uuid import UUID
from utils.conversor import Conversor

class Product_Response(BaseModel):
    id: UUID  = None
    product_image:Optional[str] = None
    name:str
    description:str
    quantity:int
    price:float
    warning_stock:int
    um_name:str
    product_type_name:str
    created_at:datetime
    active:bool

    class Config:
        orm_mode=True

class Product_Request(BaseModel):
    product_image:Optional[str] = None
    name:str
    description:str
    quantity:int
    price:float
    warning_stock:int
    um_uuid: UUID
    product_type_uuid:UUID
    active:bool

    class Config:
        orm_mode=True
