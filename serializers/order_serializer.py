from datetime import datetime, timedelta
from typing import List, Optional
from pydantic import BaseModel
from uuid import UUID

class Order_Response(BaseModel):
    id: UUID  = None
    entry_date:datetime
    delivery_date:datetime
    customer_id:UUID
    customer_name:str
    order_type:str
    order_status:str
    total:float
    created_at:datetime
    active:bool

    class Config:
        orm_mode=True

class Order_Detail_Response(BaseModel):
    id: UUID  = None
    product_uuid:UUID
    product_name:str
    price:float
    quantity:int
    created_at:datetime
    active:Optional[bool]

    class Config:
        orm_mode=True

class Order_Detail_Request(BaseModel):
    product_uuid:UUID
    price:float
    quantity:int

    class Config:
        orm_mode=True

class Order_Request(BaseModel):
    entry_date:Optional[datetime] = datetime.now()
    delivery_date:Optional[datetime] = datetime.now()+ timedelta(hours=1)
    customer_uuid:UUID
    detail:List[Order_Detail_Request]
    order_type_uuid:UUID
    status_uuid:UUID
    total:float
    class Config:
        orm_mode=True

class Order_Pdf_Response(BaseModel):
    content:str
    class Config:
        orm_mode=True