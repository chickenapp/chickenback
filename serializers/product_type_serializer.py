from datetime import datetime
from typing import Optional
from uuid import UUID
from pydantic import BaseModel
from uuid import UUID

class Product_Type_Request(BaseModel):
    name: str
    description: Optional[str] = None
    isStock: bool
    active: Optional[bool] = True
    class Config:
        orm_mode=True

class Product_Type_Response(BaseModel):
    id: UUID  = None
    name: str
    isStock: bool
    created_at: datetime
    active: bool

    class Config:
        orm_mode=True

class Product_Type_Search(BaseModel):
    id: Optional[UUID]  = None
    name: Optional[str] = None
    class Config:
        orm_mode=True
