from datetime import datetime
from typing import Dict, List
from pydantic import BaseModel

class Token_Response(BaseModel):
    token:str
    refresh_token:str
    expire_time:datetime
    user_uuid:str
    roles_active:Dict[str,str]

    class Config:
            orm_mode=True

class Auth_User_Response(BaseModel):
    pass

class Auth_User_Request(BaseModel):
    email:str
    password:str

    class Config:
        orm_mode=True
