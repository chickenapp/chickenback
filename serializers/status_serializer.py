from typing import Optional
from pydantic import BaseModel
from uuid import UUID

class Status_Response(BaseModel):
    id: UUID  = None
    name:str
    key_name:str
    active:bool

    class Config:
        orm_mode=True

class Status_Request(BaseModel):
    id: Optional[UUID]  = None
    name:Optional[str] = None
    active:Optional[bool]= None

    class Config:
        orm_mode=True

class Status_Search(BaseModel):
    name: str
    class Config:
        orm_mode=True
