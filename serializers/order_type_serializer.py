
from datetime import datetime
from typing import Optional
from uuid import UUID
from pydantic import BaseModel
from uuid import UUID

class Order_Type_Response(BaseModel):
    id:UUID
    name:str
    description:str
    created_at:datetime
    active:Optional[bool] = True
    class Config:
        orm_mode=True


class Order_Type_Request(BaseModel):
    name:Optional[str]
    description:Optional[str]
    class Config:
        orm_mode=True

class Order_Type_Search(BaseModel):
    name: str
    description:Optional[str] = None
    class Config:
        orm_mode=True

