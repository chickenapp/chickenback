from typing import Optional
from pydantic import BaseModel
from uuid import UUID

class Customer_Response(BaseModel):
    id: UUID  = None
    name:str
    phone:Optional[str] = None
    email:Optional[str] = None
    active:Optional[bool] = True

    class Config:
        orm_mode=True

class Customer_Request(BaseModel):
    name:str
    phone:Optional[str] = None
    email:Optional[str] = None
    active:Optional[bool] = True

    class Config:
        orm_mode=True

class Customer_Search(BaseModel):
    name:Optional[str] = None
    phone:Optional[str] = None
    email:Optional[str] = None

    class Config:
        orm_mode=True
