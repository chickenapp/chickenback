from datetime import datetime
from typing import Optional
from pydantic import BaseModel
from uuid import UUID, uuid4

class Unit_Measure_Response(BaseModel):
    id: UUID  = None
    name:str
    convert_factor:float
    created_at:datetime
    active:bool

    class Config:
        orm_mode=True

class Unit_Measure_Request(BaseModel):
    name:str
    convert_factor:Optional[float] = 0
    active:Optional[bool] = True

    class Config:
        orm_mode=True

class Unit_Measure_Search(BaseModel):
    name: str
    class Config:
        orm_mode=True