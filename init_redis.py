

import json
import time
import redis
from sqlalchemy import cast
import sqlalchemy

from database import SessionLocal
from models.customer_model import Customer
from models.order_detail_model import Order_Detail
from models.order_header_model import Order_Header
from models.product_model import Product
from models.product_type_model import Product_Type
from models.unit_measure_model import Unit_Measure

t = time.process_time()

redis_active=False

try:
    redis_client = redis.Redis(host='localhost', port=6379,db=0,decode_responses=True)
    redis_active = redis_client.ping()
except:
    print('No hay conexión al cluster REDIS')

print('Deleting Redis Cache ....')
if redis_active:
    redis_client.flushall()

db= SessionLocal()

if redis_active:
# orders
    orders_header = db.query(Order_Header).all()
    for order in orders_header:
        customer = db.query(Customer).filter(Customer.id == order.customer_uuid).first()
        redis_header ={'delivery_date': str(order.delivery_date), 'customer_id': str(customer.id), 'customer_name': customer.name}
        details = db.query(Order_Detail).filter(Order_Detail.order_header_uuid == order.id).all()
        if len(details) == 0:
            print('error',str(order.id))
            raise
        redis_detail=[]
        for detail in details:
            product_name = db.query(Product).filter(Product.id == detail.product_uuid).first()
            redis_detail.append({'product_id': str(product_name.id),'product_name': product_name.name, 'quantity': detail.quantity})
        redis_header.update({'detail': redis_detail})
        redis_client.set('order_'+ str(order.id),json.dumps(redis_header))

# customers
    customers = db.query(Customer).with_entities(   cast(Customer.id,sqlalchemy.String).label('id'),
                                                    Customer.name,
                                                    Customer.phone,
                                                    Customer.email,
                                                    cast(Customer.created_at,sqlalchemy.String).label('created_at'),
                                                    Customer.active
                                                    ).all()

    for customer in customers:
        redis_client.set('customer_'+ str(customer.id),json.dumps(customer._asdict()))

# products
    products = db.query(Product).with_entities(  cast(Product.id,sqlalchemy.String).label('id'),
                                                    Product.product_image,
                                                    Product.name,
                                                    Product.description,
                                                    Product.price,
                                                    Product.quantity,
                                                    Product.warning_stock,
                                                    cast(Product.um_uuid,sqlalchemy.String).label('um_uuid'),
                                                    cast(Product.created_at,sqlalchemy.String).label('created_at'),
                                                    Product.active
                                                    ).all()

    for product in products:
        redis_client.set('product_'+ str(product.id),json.dumps(product._asdict() ))

# units measure
    ums = db.query(Unit_Measure).with_entities(  cast(Unit_Measure.id,sqlalchemy.String).label('id'),
                                                    Unit_Measure.name,
                                                    Unit_Measure.convert_factor,
                                                    cast(Unit_Measure.created_at,sqlalchemy.String).label('created_at'),
                                                    Unit_Measure.active
                                                    ).all()

    for um in ums:
        redis_client.set('unit_measure'+ str(um.id),json.dumps(um._asdict()))

# product type
    products_type = db.query(Product_Type).with_entities(  cast(Product_Type.id,sqlalchemy.String).label('id'),
                                                    Product_Type.name,
                                                    Product_Type.description,
                                                    Product_Type.isStock,
                                                    cast(Product_Type.created_at,sqlalchemy.String).label('created_at'),
                                                    Product_Type.active
                                                    ).all()

    for product_type in products_type:
        redis_client.set('product_type_'+ str(product_type.id),json.dumps(product_type._asdict()))

print('********** Tiempo de ejecución REDIS **********  ',str(time.process_time() - t), '.segundos  ************************************************')
